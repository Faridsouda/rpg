class Personnage
{
    constructor(vie,def,defS,att,attS)
    {
        this.vie = vie
        this.def = def
        this.defS = defS
        this.att = att
        this.attS = attS
    }

    

    attaquer = function (ennemi) {
        
        let degAtt = Math.floor(Math.random() * this.att)
        let forceAtt = degAtt - ennemi.defendre()
        if (forceAtt > 0) {
            ennemi.vie -= forceAtt
            console.log("forceAtt : "+forceAtt)
            console.log("ennemi.vie : "+ennemi.vie)
        }
        else {
            console.log("attaque trop faible")
        }
        
    }
        
    defendre = function (){
        return Math.floor(Math.random() * this.def)
    }

    
}

class Hero extends Personnage
{
    constructor(vie,def,defS,att,attS)
    {
        super(vie,def,defS,att,attS)

    }

    

    


}

        
        



class Monstre extends Personnage
{
    constructor(type,vie,def,defS,att,attS)
    {
        super(vie,def,defS,att,attS)
        this.type = type // "phy" ou "mag"
    }

    
}


let hero1 = new Hero(500,15,32,55,11)
let hero2 = new Hero(500,15,32,55,11)
let hero3 = new Hero(500,15,32,55,11)
let hero4 = new Hero(500,15,32,55,11)

let monstre1 = new Monstre("phy",500,22,66,42,14)
let monstre2 = new Monstre("mag",500,22,66,42,14)
let monstre3 = new Monstre("phy",500,22,66,42,14)
let monstre4 = new Monstre("mag",500,22,66,42,14)

let listMonstres = [monstre1,monstre2,monstre3,monstre4]

